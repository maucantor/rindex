# The {Rindex} R package: Reviewing Reviewers' Rewards
------------------------------------------------
#### Mauricio Cantor (1) & Shane Gero (2)
#### (1) *Dalhousie University, Canada.* 
#### (2) *Aarhus University, Denmark*
------------------------------------------------


An R package that proposes an index for quantifying the contribution of scientific journal reviewers. The package also contains simulations to show how the index performs, using empirical and simulated data.

#### **Maintainer and contact** 

Mauricio Cantor 

<m.cantor@ymail.com>

Department of Biology, Dalhousie University

1355 Oxford St B3H 4J1 Halifax, NS, Canada